# README #

GitでローカルWindowsマシン上にリモートリポジトリ(bareリポジトリ)を作成した際にPush/Fetchができない問題を解消するためのbatファイルです。

[参考にしたサイト](http://tdf.or.jp/d/git-for-windows%E3%81%AB%E5%AF%BE%E3%81%97%E3%81%A6windows%E7%89%88openssh%E3%82%92%E4%BD%BF%E3%81%A3%E3%81%A6%E3%83%AA%E3%83%A2%E3%83%BC%E3%83%88%E3%81%8B%E3%82%89%E3%83%AA%E3%83%9D%E3%82%B8%E3%83%88/)

### What is this repository for? ###

OpenSSHを利用しPush/Fetchができない問題

### How do I get set up? ###

* `C:\Program Files\Git\cmd`にbatファイル2つを配置
* OpenSSHサーバを再起動